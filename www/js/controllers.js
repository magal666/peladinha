angular.module('starter.controllers', [])

    .controller('SorteioCtrl', function ($scope, JogadoresService, Utils, $state, $rootScope) {

        $scope.arrayJogador = [];
        $scope.arrayJogadorMarcado = [];
        $rootScope.arrayJogadoresMarcado = [];


        $scope.empty = function(valor){
            return (angular.isUndefined(valor) || valor === "" || valor === null );
        };

        $scope.buscarJogadores = function(){
            $scope.arrayJogador = JogadoresService.all();

            angular.forEach($scope.arrayJogador, function (value, key) {
                angular.forEach($rootScope.arrayJogadoresMarcado, function (value2, key2) {
                    if(value.id === value2.id){
                        value.marcado = true;
                    }
                })
            })
        }


        $scope.sortearJogadores = function(normal){


            var dados = [];
            var isEmpty = true;
            var numeroJogadores = JogadoresService.configuracoes.all();

            if($scope.empty(numeroJogadores.jogadoresPorTime)){
                alert('Antes de sortear inclua a quantidade de jogadores por time na aba configuração.');
                return false;
            }
            
            angular.forEach($scope.arrayJogador, function (value, key) {

                if(value.marcado){
                    dados.push(value);
                }

            });

            for (var i in dados) {
                if(dados.hasOwnProperty(i)) {
                    isEmpty = false;
                    break;
                }
            }

            if(isEmpty){
                alert('Você deve marcar algum jogador para realizar o sorteio.');
                return false;
            }

            var sorteados = [];

            if(normal){
                sorteados = Utils.getRandom(dados, dados.length);
            }else{
                var estrela1 = [];
                var estrela2 = [];
                var estrela3 = [];
                var estrela4 = [];
                var estrela5 = [];
                var estrela6 = [];
                var estrela7 = [];
                var estrela8 = [];
                var estrela9 = [];
                var estrela10 = [];


                angular.forEach(dados, function (value, key) {

                    if(value.estrela == 10){
                        estrela10.push(value);
                    }else if(value.estrela == 9){
                        estrela9.push(value);
                    }else if(value.estrela == 8){
                        estrela8.push(value);
                    }else if(value.estrela == 7){
                        estrela7.push(value);
                    }else if(value.estrela == 6){
                        estrela6.push(value);
                    }else if(value.estrela == 5){
                        estrela5.push(value);
                    }else if(value.estrela == 4){
                        estrela4.push(value);
                    }else if(value.estrela == 3){
                        estrela3.push(value);
                    }else if(value.estrela == 2){
                        estrela2.push(value);
                    }else if(value.estrela == 1){
                        estrela1.push(value);
                    }

                });

                estrela10 = Utils.getRandom(estrela10, estrela10.length);
                estrela9 = Utils.getRandom(estrela9, estrela9.length);
                estrela8 = Utils.getRandom(estrela8, estrela8.length);
                estrela7 = Utils.getRandom(estrela7, estrela7.length);
                estrela6 = Utils.getRandom(estrela6, estrela6.length);
                estrela5 = Utils.getRandom(estrela5, estrela5.length);
                estrela4 = Utils.getRandom(estrela4, estrela4.length);
                estrela3 = Utils.getRandom(estrela3, estrela3.length);
                estrela2 = Utils.getRandom(estrela2, estrela2.length);
                estrela1 = Utils.getRandom(estrela1, estrela1.length);

                var jogadoresSorteados = [];

                angular.forEach(estrela10, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela9, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela8, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela7, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela6, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela5, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela4, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela3, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela2, function (value, key) {
                    jogadoresSorteados.push(value);
                })

                angular.forEach(estrela1, function (value, key) {
                    jogadoresSorteados.push(value);
                })



                sorteados = jogadoresSorteados;
            }

            $state.go('tab.sorteados', {dados: sorteados, sorteioSimples: normal });

        }

        $scope.desmarcarTodos = function (){

            angular.forEach($scope.arrayJogador, function (value, key) {

                value.marcado = false;

            })

        }

        $scope.marcarTodos = function (){

            angular.forEach($scope.arrayJogador, function (value, key) {

                value.marcado = true;

            })

        }




        $scope.$on('$ionicView.enter', function () {
            //$scope.arrayJogador = [];
            //$scope.arrayJogadorMarcado = [];
            $scope.buscarJogadores();
        });

        $scope.contagemJogadores = function (dados) {

            if($scope.empty(dados)){

                angular.forEach($scope.arrayJogador, function (value, key) {

                    if(value.marcado){

                        if($scope.arrayJogador.length > $scope.arrayJogadorMarcado.length){
                            $scope.arrayJogadorMarcado.push(value);
                        }

                    }else{

                        if($scope.arrayJogadorMarcado.length !== 0){

                            $scope.arrayJogadorMarcado.splice(value, 1);

                        }
                    }

                })

            }else{

                console.log(dados);
                console.log($scope.arrayJogadorMarcado);

                if(dados.marcado){
                    $scope.arrayJogadorMarcado.push(dados);
                }else{
                    angular.forEach($scope.arrayJogadorMarcado, function (value, key) {
                        if(dados.id === value.id){
                            $scope.arrayJogadorMarcado.splice(key, 1);
                        }
                    })
                }

            }

            $rootScope.arrayJogadoresMarcado = $scope.arrayJogadorMarcado;

        }

    })

    .controller('SorteadosCtrl', function ($scope, JogadoresService, $stateParams) {

        Array.prototype.chunk = function (groupsize) {
            var sets = [];
            var chunks = this.length / groupsize;

            for (var i = 0, j = 0; i < chunks; i++, j += groupsize) {
                sets[i] = this.slice(j, j + groupsize);
            }

            return sets;
        };

        $scope.empty = function(valor){
            return (angular.isUndefined(valor) || valor === "" || valor === null );
        };

        $scope.estrelas = true;
        $scope.arrayJogadoresSorteados = [];
        $scope.numeroJogadores = JogadoresService.configuracoes.all();
        $scope.maximoEstrelas = $scope.numeroJogadores.quantidadeEstrela;

        $scope.mostrarOcultarEstrelas = function () {
            if($scope.estrelas){
                $scope.estrelas = false;
            }else{
                $scope.estrelas = true;
            }
        }

        $scope.criarTela = function () {

            var dados = $stateParams.dados;
            var sorteioSimples = $stateParams.sorteioSimples;

            if(sorteioSimples){
                $scope.arrayJogadoresSorteados = dados.chunk($scope.numeroJogadores.jogadoresPorTime);
            }else{

                var mediaTimes = dados.length / $scope.numeroJogadores.jogadoresPorTime;
                var quantidadeTime = Math.ceil(mediaTimes);
                var quantidadeTimeIncompleto = dados.length % $scope.numeroJogadores.jogadoresPorTime;
                var arrayTimes = [];
                var arrayPontuacaoPorTime = [];

                for(var i = 0; i < quantidadeTime; i++) {
                    arrayTimes[i] = [];
                    arrayPontuacaoPorTime[i] = 0;
                }

                function defineTime() {
                    var menor = 9999999;
                    var menorIndice = 0;

                    console.log("arrayPontuacaoPorTime", arrayPontuacaoPorTime);
                    angular.forEach(arrayPontuacaoPorTime, function (mediaTime, indiceTime) {

                        if(mediaTime < menor){
                            menorIndice = indiceTime;
                            menor = mediaTime;
                        }
                    });

                    return menorIndice;
                }

                function recalculaPeso () {

                    angular.forEach(arrayTimes, function (value, key) {

                        var soma = 0;

                        angular.forEach(value, function (value2) {
                            soma += value2.estrela;
                        })

                        if(key == (arrayTimes.length - 1) && quantidadeTimeIncompleto > 0){
                            arrayPontuacaoPorTime[key] = soma / quantidadeTimeIncompleto;
                        }else{
                            arrayPontuacaoPorTime[key] = soma / $scope.numeroJogadores.jogadoresPorTime;
                        }
                    })

                }

                angular.forEach(dados, function (value,key) {

                    var indiceEscolhido = defineTime();
                    console.log("indiceEscolhido", indiceEscolhido);
                    arrayTimes[indiceEscolhido].push(value);
                    recalculaPeso();

                })

                $scope.arrayJogadoresSorteados = arrayTimes;
                console.log($scope.arrayJogadoresSorteados);


            }

        }

        $scope.$on('$ionicView.enter', function () {
            $scope.arrayJogadoresSorteados = {};
            $scope.numeroJogadores = JogadoresService.configuracoes.all();
            $scope.criarTela();
        });



    })

    .controller('JogadoresCtrl', function ($scope, JogadoresService, $filter) {

        $scope.inputData = {
            jogador: undefined,
        };
        $scope.arrayJogador = [];
        //$scope.maximo = 5;
        //location.reload();

        var configuracoes = JogadoresService.configuracoes.all();
        $scope.maximo = configuracoes.quantidadeEstrela;


        $scope.empty = function(valor){
            return (angular.isUndefined(valor) || valor === "" || valor === null );
        };

        $scope.adicionarJogador = function (jogador) {

            if($scope.empty(jogador)){
                alert('Insira o nome do jogador para continuar.');
                return false;
            }

            var jogadorExistente = false;
            angular.forEach($scope.arrayJogador, function (value, key) {

                if(value.nome.toLowerCase() == jogador.toLowerCase()){
                    jogadorExistente = true;
                }

            });

            if(jogadorExistente){
                alert('Já existe um jogador com este nome.');
                return false;
            }

            JogadoresService.add(jogador);
            $scope.limparCampo();
            $scope.buscarJogadores();

        }

        $scope.removerJogador = function(jogador){

            if(!confirm("Deseja remover este jogador?")){
                return false;
            }

            console.log(jogador);

            JogadoresService.remove(jogador)
            $scope.buscarJogadores();

        }

        $scope.buscarJogadores = function(){
            $scope.arrayJogador = JogadoresService.all();
        }

        $scope.buscarJogadores();

        $scope.limparCampo = function () {
            $scope.inputData.jogador = undefined;
        }

        $scope.alterarEstrelas = function (jogador) {

            JogadoresService.edit(jogador);

        }

    })

    .controller('ConfiguracoesCtrl', function ($scope, JogadoresService) {

        $scope.inputData = {
            jogadoresPorTime: 0,
            quantidadeEstrela: 5,
            pelada: undefined
        }
        $scope.arrayPelada = [];

        $scope.empty = function(valor){
            return (angular.isUndefined(valor) || valor === "" || valor === null );
        };

        $scope.buscarConfiguracoes = function () {

            var configuracoes = JogadoresService.configuracoes.all();
            $scope.inputData.jogadoresPorTime = configuracoes.jogadoresPorTime;
            $scope.inputData.quantidadeEstrela = !$scope.empty(configuracoes.quantidadeEstrela) ? configuracoes.quantidadeEstrela : $scope.inputData.quantidadeEstrela;
        }

        $scope.buscarConfiguracoes();

        $scope.salvarConfiguracoes = function (jogadoresPorTime, quantidadeEstrela) {

            JogadoresService.configuracoes.jogadoresPorTime(jogadoresPorTime);
            JogadoresService.configuracoes.quantidadeEstrela(quantidadeEstrela);
            alert('Configuração salva com sucesso!');
            location.reload();

        }

        $scope.adicionarPelada = function (nome) {
            JogadoresService.configuracoes.pelada.add(nome);
            $scope.buscarPeladas();
            $scope.inputData.pelada = undefined;
        }

        $scope.buscarPeladas = function () {
            $scope.arrayPelada = JogadoresService.configuracoes.pelada.all();
        }

        //$scope.buscarPeladas();

        $scope.removerPelada = function (pelada) {
            if(!confirm("Deseja remover a pelada?")){
                return false;
            }

            JogadoresService.configuracoes.pelada.remove(pelada);
            $scope.buscarPeladas();
        }

    });
