angular.module('starter.services', [])

    .factory('$localStorage', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            }
        }
    }])

    .factory('JogadoresService', function ($localStorage) {

        /*if(!$localStorage.getObject('configuracoes', '{}')){
            var configuracoes = {
                jogadoresPorTime: undefined
            };

            $localStorage.setObject(configuracoes);
        }*/

        //função responsável por fazer o sort() funcionar para inteiro
        function sortNumber(a, b) {
            return a - b;
        }

        return {
            all: function () {
                var jogadores = $localStorage.getObject('jogadores', '{}');
                var jogadoresRetorno = [];
                angular.forEach(jogadores, function (value, key) {
                    jogadoresRetorno.push(value);
                });

                return jogadoresRetorno;
            },
            remove: function (jogador) {
                var jogadores = $localStorage.getObject('jogadores', '{}');
                delete jogadores[jogador.id];
                $localStorage.setObject('jogadores', jogadores);
            },
            add: function (jogador) {
                var jogadores = $localStorage.getObject('jogadores', '{}');
                var id = 0;
                if (Object.keys(jogadores).length > 0) {
                    var jogadoresArray = Object.keys(jogadores);
                    var jogadoresSort = jogadoresArray.sort(sortNumber);
                    var ultimoItem = jogadoresSort[jogadoresSort.length - 1];
                    id = parseInt(ultimoItem) + 1;
                } else {
                    var id = 1;
                }

                /*console.log("jogadores: ", jogadores);
                console.log("ID: ", id);*/
                //var id = Object.keys(jogadores).length + 1;
                jogadores[id] = {id: id, nome: jogador, marcado: false, estrela: 1};
                $localStorage.setObject('jogadores', jogadores);
            },
            edit: function (jogador) {
                var jogadores = $localStorage.getObject('jogadores', '{}');
                jogadores[jogador.id] = jogador;
                $localStorage.setObject('jogadores', jogadores);
            },
            configuracoes: {
                jogadoresPorTime: function (valor) {
                    var configuracoes = $localStorage.getObject('configuracoes', '{}');
                    configuracoes.jogadoresPorTime = valor;

                    $localStorage.setObject('configuracoes', configuracoes);
                },
                quantidadeEstrela: function (valor) {
                    var configuracoes = $localStorage.getObject('configuracoes', '{}');
                    configuracoes.quantidadeEstrela = valor;

                    $localStorage.setObject('configuracoes', configuracoes);
                },
                all: function () {
                    return $localStorage.getObject('configuracoes', '{}');
                },

                /*pelada: {
                    all: function(){
                        var peladas = $localStorage.getObject('peladas', '{}');
                        var peladasRetorno = [];
                        angular.forEach(peladas,function(value,key){
                            peladasRetorno.push(value);
                        });

                        console.log(peladasRetorno);
                        return peladasRetorno;

                    },
                    add: function (nomePelada) {
                        var peladas = $localStorage.getObject('peladas', '{}');
                        var id = 0;
                        if(Object.keys(peladas).length > 0){
                            var peladaArray = Object.keys(peladas);
                            var peladaSort = peladaArray.sort();
                            var ultimoItemPelada = peladaSort[peladaSort.length - 1];
                            id = parseInt(ultimoItemPelada) + 1;
                        }else{
                            var id = 1;
                        }
                        //var id = Object.keys(jogadores).length + 1;
                        peladas[id] = {id: id, nome: nomePelada};
                        $localStorage.setObject('peladas', peladas);
                    },
                    remove: function (pelada) {
                        var peladas = $localStorage.getObject('peladas', '{}');
                        delete peladas[pelada.id];
                        $localStorage.setObject('peladas', peladas);
                    }
                },*/

            }
        };
    });
